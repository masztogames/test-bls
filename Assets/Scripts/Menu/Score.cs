using TMPro;
using UnityEngine;

namespace TJ.BLS.Test
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _topScore;
        [SerializeField] private TextMeshProUGUI _lastScore;
        [SerializeField] private TempData _tempData;
        [SerializeField] private GameObject _lastScoreObject;

        private DataManager _dataManager;

        void Start()
        {
            _dataManager = FindObjectOfType<DataManager>();
            _dataManager.Load();
            _lastScoreObject.SetActive(_tempData.HasPlayed);
            _topScore.text = _dataManager.Data.TopPoints.ToString();
            _lastScore.text = _dataManager.Data.LastPoints.ToString();
        }
    }
}
