using UnityEngine;
using UnityEngine.SceneManagement;

namespace TJ.BLS.Test
{
    public class MenuInput : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
            {
                return;
            }
            if (Input.anyKey)
            {
                SceneManager.LoadScene(1);
            }
        }

        public void ExidGame()
        {
            Application.Quit();
        }
    }
}
