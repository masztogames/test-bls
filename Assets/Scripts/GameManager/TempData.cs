using UnityEngine;

namespace TJ.BLS.Test
{
    [CreateAssetMenu]
    public class TempData : ScriptableObject
    {
        public int CurrentPoints;
        public int CurrentLifes;
        public bool HasPlayed;
    }
}
