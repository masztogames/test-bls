using UnityEngine;
using UnityEngine.SceneManagement;

namespace TJ.BLS.Test
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private TempData _tempData;
        [SerializeField] private int _playerLifes;

        private DataManager _saveData;

        public delegate void GameUpdates();
        public static event GameUpdates OnUiUpdate;

        void OnEnable()
        {
            PlayerHealth.OnPlayerDie += Die;
            Enemy.OnEnemyDie += AddPoints;
            GameTimer.OnTimeEnd += GameOver;
        }

        private void OnDisable()
        {
            PlayerHealth.OnPlayerDie -= Die;
            Enemy.OnEnemyDie -= AddPoints;
            GameTimer.OnTimeEnd -= GameOver;
        }

        void Start()
        {
            _tempData.CurrentLifes = _playerLifes;
            _tempData.CurrentPoints = 0;
            _saveData = GetComponent<DataManager>();
        }

        private void Die()
        {
            _tempData.CurrentLifes--;
            OnUiUpdate();
            if (_tempData.CurrentLifes <= 0)
            {
                _tempData.CurrentLifes = 0;
                GameOver();
            }
        }

        private void AddPoints()
        {
            _tempData.CurrentPoints++;
            OnUiUpdate();
        }

        private void GameOver()
        {
            _saveData.Data.LastPoints = _tempData.CurrentPoints;
            _saveData.Data.TopPoints = GetHigherScore();
            _tempData.HasPlayed = true;
            _saveData.Save();
            Debug.Log(SceneManager.GetSceneAt(0).name);
            SceneManager.LoadScene(0);
        }

        private int GetHigherScore()
        {
            return _saveData.Data.TopPoints > _tempData.CurrentPoints ? _saveData.Data.TopPoints : _tempData.CurrentPoints;
        }
    }
}
