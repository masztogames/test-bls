using TMPro;
using UnityEngine;

namespace TJ.BLS.Test
{
    public class UiManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _pointsText;
        [SerializeField] private TextMeshProUGUI _lifesText;
        [SerializeField] private TempData _tempData;

        private void OnEnable()
        {
            GameManager.OnUiUpdate += UpdateUI;
        }

        private void OnDisable()
        {
            GameManager.OnUiUpdate -= UpdateUI;
        }

        private void Start()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            _pointsText.text = _tempData.CurrentPoints.ToString();
            _lifesText.text = _tempData.CurrentLifes.ToString();
        }
    }
}
