using TMPro;
using UnityEngine;

namespace TJ.BLS.Test
{
    public class GameTimer : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private float _gameTime;
        private float _timer;

        public delegate void GameUpdates();
        public static event GameUpdates OnTimeEnd;

        void Update()
        {
            _timer += Time.deltaTime;
            _timerText.text = _timer.ToString();
            if (_timer >= _gameTime)
            {
                OnTimeEnd();
            }
        }
    }
}
