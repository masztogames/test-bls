using UnityEngine;

namespace TJ.BLS.Test
{
    public class Background : MonoBehaviour
    {
        [SerializeField] private float _scrollingSpeed;
        private Renderer _renderer;

        void Start()
        {
            _renderer = GetComponent<Renderer>();
        }

        void Update()
        {
            Vector2 offset = new Vector2(Time.time * _scrollingSpeed, 0);
            _renderer.material.mainTextureOffset = offset;
        }
    }
}
