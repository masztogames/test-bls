using UnityEngine;

namespace TJ.BLS.Test
{
    public class Enemy : MonoBehaviour
    {
        public delegate void EnemyDie();
        public static event EnemyDie OnEnemyDie;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag("Bullet"))
            {
                OnEnemyDie();
            }

            if (collision.transform.CompareTag("Enemy"))
            {
                return;
            }

            gameObject.SetActive(false);
        }
    }
}
