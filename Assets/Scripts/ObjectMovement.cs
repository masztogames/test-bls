using UnityEngine;

namespace TJ.BLS.Test
{
    public class ObjectMovement : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private bool _moveRight;

        private void Start()
        {
            _speed = _moveRight ? _speed : -_speed;
        }

        void Update()
        {
            transform.Translate(new Vector3(1, 0) * _speed * Time.deltaTime);
        }
    }
}
