using UnityEngine;

namespace TJ.BLS.Test
{
    public class PlayerHealth : MonoBehaviour
    {
        public delegate void PlayerDie();
        public static event PlayerDie OnPlayerDie;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnPlayerDie();
        }
    }
}
