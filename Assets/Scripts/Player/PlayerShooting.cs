using UnityEngine;

namespace TJ.BLS.Test
{
    public class PlayerShooting : MonoBehaviour
    {
        private ObjectPool _pool;

        private void Awake()
        {
            _pool = GetComponentInChildren<ObjectPool>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject obj = _pool.GetPooledObject();
                obj.SetActive(true);
                obj.transform.position = transform.position;
                obj.transform.parent = null;
            }
        }
    }
}
