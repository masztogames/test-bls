using UnityEngine;

namespace TJ.BLS.Test
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _speed;
        private Vector3 _playerPosition;
        private float _screenHeight;

        float screenBottomLine;
        float screenTopLine;

        private void Start()
        {
            _playerPosition = transform.position;
            _screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
            screenBottomLine = transform.position.y - _screenHeight / 2;
            screenTopLine = transform.position.y + _screenHeight / 2;
        }

        void Update()
        {
            _playerPosition.y += Input.GetAxis("Vertical") * _speed * Time.deltaTime;
            _playerPosition.y = Mathf.Clamp(_playerPosition.y, screenTopLine, screenBottomLine);
            transform.position = _playerPosition;
        }
    }
}
