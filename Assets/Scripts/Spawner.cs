using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float _timeBetweenSpan = 4f;
    [SerializeField] private int _enemiesPerWaveMinValue = 2;
    [SerializeField] private int _enemiesPerWaveMaxValue = 5;
    [SerializeField] private int _numberOfTracks = 5;

    private ObjectPool _pool;
    private float _timer;
    private float _screenHeight;

    private List<float> _spawnPoint = new List<float>();

    private void Awake()
    {
        _pool = GetComponentInChildren<ObjectPool>();
        _screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
    }

    void Start()
    {
        float centerY = (_screenHeight / _numberOfTracks) /2;
        float screenBottomLine = transform.position.y - _screenHeight / 2;
        float firstPosition = screenBottomLine - centerY;

        for(int i=0;i < _numberOfTracks; i++)
        {
            firstPosition = firstPosition + (_screenHeight / _numberOfTracks);
            _spawnPoint.Add(firstPosition);
        }

        if(_enemiesPerWaveMinValue > _enemiesPerWaveMaxValue)
        {
            _enemiesPerWaveMinValue = _enemiesPerWaveMaxValue - 1;
        }
    }

    void Update()
    {
        _timer += Time.deltaTime;

        if(_timer >= _timeBetweenSpan)
        {
            _timer = 0f;
            SpawnWave();
        }
    }

    void SpawnWave()
    {
        int random = Random.Range(_enemiesPerWaveMinValue, _enemiesPerWaveMaxValue);

        for(int i=0;i < random; i++)
        {
            GameObject obj = _pool.GetPooledObject();
            obj.SetActive(true);
            obj.transform.position = GetRandomPosition();
        }
    }

    private Vector3 GetRandomPosition()
    {
        int random = Random.Range(0, _numberOfTracks);
        float randomValueY = _spawnPoint[random];

        return new Vector3(transform.position.x, randomValueY, transform.position.z);
    }
}
