
# Recruitment Test BLS

Recruitment task for BLS, a project created using the assets provided with the task.


## Features

- The spawner calculates the positions on which the opponents are to appear based on the number of tracks that we specify in the inspector,
- Saving score using JsonUtility, 
- Object pooling to limit resources,
- Exit Game button :)


## Author

- [@T.Janicki](https://www.linkedin.com/in/tomasz-janicki-809a641b9/)

## Demo

- [Link](https://we.tl/t-hwbok4uhab)



## Task time

 - Working on this project took me about 5.5 hours.
